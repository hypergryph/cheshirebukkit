package com.aqmoe.cheshire.managers;

import com.aqmoe.aquakits.exceptions.HonMeirin;
import com.aqmoe.cheshire.Cheshire;
import com.aqmoe.cheshire.managers.module.CheshireModule;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ConfigManager {
    static FileConfiguration configuration;
    static CheshireConfig cheshireConfig;
    public static void load() {
        Cheshire.getBukkitLogger().info("Loading configuration...");
        Cheshire.getInstance().saveDefaultConfig();

        configuration = Cheshire.getInstance().getConfig();

        if(Cheshire.config_version != configuration.getInt("version")) {
            Cheshire.getBukkitLogger().warning("Your configuration needs to be updated. We are working on it...");
            File ourConfigFile = new File(Cheshire.getInstance().getDataFolder(), "config.yml");
            File backupFile = new File(Cheshire.getInstance().getDataFolder(), "config.backup.yml");
            try {
                Files.move(ourConfigFile.toPath(), backupFile.toPath());
                Cheshire.getInstance().saveDefaultConfig();
                Cheshire.getInstance().reloadConfig();
                configuration = Cheshire.getInstance().getConfig();
                Cheshire.getBukkitLogger().warning("We've successfully update your configuration!");
            } catch (IOException e) {
                HonMeirin.hold(e);
            }

        }

        cheshireConfig = new CheshireConfig();
        cheshireConfig.DEBUG = getConfig().getBoolean("debug");
    }

    public static CheshireConfig getCheshireConfig() {
        return cheshireConfig;
    }

    public static FileConfiguration getConfig() {
        return configuration;
    }

    public static ConfigurationSection getModuleConfig(String string) {
        if(configuration.contains(string)) {
            return configuration.getConfigurationSection(string);
        } else {
            throw new RuntimeException("Cannot get config of module " + string + "!");
        }
    }

    public static ConfigurationSection getModuleConfig(CheshireModule module) {
        return getModuleConfig(module.getName());
    }

    public static void reloadAll() {
        Cheshire.getInstance().reloadConfig();
        load();
    }
}

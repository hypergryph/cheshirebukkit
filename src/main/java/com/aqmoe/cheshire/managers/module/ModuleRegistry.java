package com.aqmoe.cheshire.managers.module;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ModuleRegistry {
    boolean sync() default false;
    boolean system() default false;
    String name() default "";
}

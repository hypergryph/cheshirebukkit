package com.aqmoe.cheshire.managers.module;

import com.aqmoe.cheshire.Cheshire;
import com.aqmoe.cheshire.managers.ConfigManager;

import java.text.MessageFormat;
import java.util.logging.Logger;

public class ModuleLogger {
    String name;
    Logger logger;
    String last_debug_message;
    int loop_message_count = 0;
    public ModuleLogger(CheshireModule module) {
        name = module.getName();
        logger = Cheshire.getInstance().getLogger();
    }
    String getFormat(String message) {
        return MessageFormat.format("[{0}] {1}", name, message);
    }
    public void debug(String message) {
        if(ConfigManager.getCheshireConfig().DEBUG) {
            if(last_debug_message != null) {
                if(last_debug_message.equalsIgnoreCase(message)) {
                    loop_message_count ++;
                    return;
                } else if (loop_message_count > 0) {
                    // 这一条消息和上一条不一样；而且上一条重复了不止一次
                    logger.info(getFormat("(debug) (+" + loop_message_count + ") " + last_debug_message));
                    loop_message_count = 0;
                }
            }

            logger.info(getFormat("(debug) " + message));
            last_debug_message = message;
        }
    }
    public void info(String message) {
        logger.info(getFormat(message));
    }
    public void warning(String message) {
        logger.warning(getFormat(message));
    }
    public void severe(String message) {
        logger.severe(getFormat(message));
    }
}

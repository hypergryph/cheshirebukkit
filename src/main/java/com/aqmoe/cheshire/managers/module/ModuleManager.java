package com.aqmoe.cheshire.managers.module;

import com.aqmoe.aquakits.exceptions.HonMeirin;
import com.aqmoe.cheshire.Cheshire;
import com.aqmoe.cheshire.managers.ConfigManager;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Set;

public class ModuleManager {
    static HashMap<String, CheshireModule> moduleHashMap = new HashMap<>();
    public static void loadAllPackage(String p) {
        Set<Class<?>> modules = new Reflections(p).getTypesAnnotatedWith(ModuleRegistry.class);
        for(Class<?> m : modules) {
            if(m.isAnnotationPresent(ModuleRegistry.class)) {
                ModuleRegistry moduleRegistry = (ModuleRegistry) m.getAnnotation(ModuleRegistry.class);
                boolean isSystem = moduleRegistry.system();
                boolean isSync = moduleRegistry.sync();
                String name = moduleRegistry.name();

                if(name.isEmpty() || name.equalsIgnoreCase("")) {
                    name = m.getSimpleName();
                }

                if(isSystem || ConfigManager.getModuleConfig(name).getBoolean("enabled")) {
                    try {
                        CheshireModule cheshireModule = (CheshireModule) m.newInstance();
                        cheshireModule.setName(name);
                        cheshireModule.setFullySync(isSync);
                        cheshireModule.setSystemLevel(isSystem);
                        cheshireModule.initLogger();
                        moduleHashMap.put(name, cheshireModule);

                        Cheshire.getBukkitLogger().info("Loading module " + name);
                        cheshireModule.onEnable();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public static void unloadAll() {
        for(CheshireModule module : moduleHashMap.values()) {
            unload(module);
        }
    }

    public static void unload(CheshireModule module) {
        try {
            Cheshire.getBukkitLogger().info("Unloading module " + module.getName());
            module.onDisable();
        } catch (Exception e) {
            HonMeirin.hold(e);
        }
    }

    public static CheshireModule getModule(String string) {
        return moduleHashMap.get(string);
    }
}

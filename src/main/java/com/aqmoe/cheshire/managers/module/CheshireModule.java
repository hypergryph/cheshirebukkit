package com.aqmoe.cheshire.managers.module;

import com.aqmoe.cheshire.managers.ConfigManager;
import com.aqmoe.cheshire.managers.event.EventParameters;
import com.aqmoe.cheshire.managers.event.EventType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.Event;

public abstract class CheshireModule {
    public String name;
    public boolean fullySync;
    public boolean systemLevel;
    ModuleLogger logger;

    public final String getName() {
        return name;
    }

    public final void setName(String name) {
        this.name = name;
    }

    public final void setFullySync(boolean fullySync) {
        this.fullySync = fullySync;
    }

    public final void setSystemLevel(boolean systemLevel) {
        this.systemLevel = systemLevel;
    }

    public final void initLogger() {
        logger = new ModuleLogger(this);
    }

    public final boolean isFullySync() {
        return fullySync;
    }

    public final boolean isSystemLevel() {
        return systemLevel;
    }

    public void onEnable() {

    }

    public void onDisable() {

    }

    public void onListening(EventType type, Event bukkit_event, EventParameters parameters) {

    }

    public final ModuleLogger getLogger() {
        return logger;
    }

    public final ConfigurationSection getConfig() {
        return ConfigManager.getModuleConfig(this);
    }

}

package com.aqmoe.cheshire.managers;

import com.aqmoe.cheshire.Cheshire;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;

public class DependenciesManager {
    boolean OK = true;
    HashMap<String, Plugin> dependenciesInstance;
    public DependenciesManager() {
        dependenciesInstance = new HashMap<>();
    }

    public boolean isOK() {
        return OK;
    }

    public Plugin getInstance(String name) {
        return dependenciesInstance.get(name);
    }

    public void getPlugin(String name, boolean required) {
        if(Bukkit.getPluginManager().isPluginEnabled(name)) {
            Plugin plugin = Bukkit.getPluginManager().getPlugin(name);
            dependenciesInstance.put(name, plugin);
            return;
        }
        dependenciesInstance.put(name, null);
        if(required) {
            Cheshire.getBukkitLogger().severe(" ");
            Cheshire.getBukkitLogger().severe(name + " are required to run Cheshire.");
            Cheshire.getBukkitLogger().severe("Please first put " + name + " to your plugins dir. Cheshire will now self-disable.");
            Cheshire.getBukkitLogger().severe(" ");
            Cheshire.getInstance().getPluginLoader().disablePlugin(Cheshire.getInstance());
            OK = false;
            return;
        }
    }
}

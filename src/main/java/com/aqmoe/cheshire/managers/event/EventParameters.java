package com.aqmoe.cheshire.managers.event;

import java.util.HashMap;

public class EventParameters {
    HashMap<String, Object> parameters;

    public EventParameters() {
        parameters = new HashMap<>();
    }

    public EventParameters add(String str, Object obj) {
        parameters.put(str, obj);
        return this;
    }

    public Object get(String str) {
        return parameters.get(str);
    }
}

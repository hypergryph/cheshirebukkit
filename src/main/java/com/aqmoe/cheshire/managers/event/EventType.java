package com.aqmoe.cheshire.managers.event;

public enum EventType {
    ServerInDanger,
    CreaturePreSpawn,
    CreatureSpawn,
    HopperMoveItem,
    WorldLoad,
    EntityDeath,
    CreatureCategoryReachedLimit
}

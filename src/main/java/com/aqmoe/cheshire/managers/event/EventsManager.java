package com.aqmoe.cheshire.managers.event;

import com.aqmoe.cheshire.Cheshire;
import com.aqmoe.cheshire.managers.module.CheshireModule;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import java.util.ArrayList;
import java.util.HashMap;

public class EventsManager {
    public static HashMap<EventType, ArrayList<CheshireModule>> listen_modules = new HashMap<>();

    /**
     * Register module to listen to specific type of event.
     * @param type Type of event
     * @param module Module
     */
    public static void register(EventType type, CheshireModule module) {
        ArrayList<CheshireModule> newList;
        if (listen_modules.containsKey(type)) {
            newList = listen_modules.get(type);
        } else {
            newList = new ArrayList<>();
        }
        newList.add(module);
        listen_modules.put(type, newList);
    }

    /**
     * Triggers the specified type of listener
     * @param type Type of the event
     * @param event Bukkit event object
     * @param parameters Parameters provided for the listeners
     */
    public static void fire(EventType type, Event event, EventParameters parameters) {

        // trying to invoke all the listeners asynchronously
        // module can avoid this by flagging isFullySync()

        Bukkit.getScheduler().runTaskAsynchronously(Cheshire.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (listen_modules.containsKey(type)) {
                    for (CheshireModule module : listen_modules.get(type)) {
                        try {
                            if (module.isFullySync()) {
                                Bukkit.getScheduler().runTask(Cheshire.getInstance(), new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            module.onListening(type, event, parameters);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            } else {
                                // just invoke it in async way
                                module.onListening(type, event, parameters);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    public static void fire(EventType type) {
        fire(type, null, null);
    }
}

package com.aqmoe.cheshire.managers.event;

import com.bergerkiller.bukkit.common.events.CreaturePreSpawnEvent;
import org.bukkit.block.Hopper;
import org.bukkit.entity.minecart.HopperMinecart;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.world.WorldLoadEvent;

public class BukkitEventAdopter implements Listener {
    @EventHandler
    public void A(CreaturePreSpawnEvent event) {
        EventsManager.fire(EventType.CreaturePreSpawn, event, null);
    }
    @EventHandler
    public void B(InventoryMoveItemEvent event) {
        if (event.getSource().getHolder() instanceof Hopper || event.getSource().getHolder() instanceof HopperMinecart) {
            EventsManager.fire(EventType.HopperMoveItem, event, null);
        }
    }
    @EventHandler
    public void C(WorldLoadEvent event) {
        EventsManager.fire(EventType.WorldLoad, event, null);
    }
    @EventHandler
    public void D(CreatureSpawnEvent event) {
        EventsManager.fire(EventType.CreatureSpawn, event, null);
    }
    @EventHandler
    public void E(EntityDeathEvent event) {
        EventsManager.fire(EventType.EntityDeath, event, null);
    }
}

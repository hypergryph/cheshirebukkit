package com.aqmoe.cheshire.impl.category;

import com.aqmoe.cheshire.Cheshire;
import com.aqmoe.cheshire.managers.module.ModuleManager;
import org.bukkit.entity.*;

public class CreatureCategories {
    public static CreatureCategory recognize(Entity entity) {

        // Put WaterMob before Animals.
        if(entity instanceof WaterMob) {
            return CreatureCategory.WaterAnimal;
        }
        if(entity instanceof Ambient) {
            return CreatureCategory.Misc;
        }

        if(entity instanceof Animals) {
            return CreatureCategory.Animal;
        }
        if(entity instanceof Monster) {
            return CreatureCategory.Monster;
        }
        if(entity instanceof Projectile) {
            return CreatureCategory.Projectile;
        }
        return CreatureCategory.NotRecognized;
    }
}

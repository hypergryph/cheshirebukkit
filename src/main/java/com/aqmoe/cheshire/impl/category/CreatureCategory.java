package com.aqmoe.cheshire.impl.category;

public enum CreatureCategory {
    Animal,
    WaterAnimal,
    Monster,
    Misc,
    NotRecognized,
    Projectile
}

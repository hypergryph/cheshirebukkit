package com.aqmoe.cheshire.impl.category;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Tameable;

import java.util.Locale;

public class SafetyCheck {
    public static boolean isEntityTypeRemovable(EntityType type) {
        String entityType = type.name();
        switch (entityType.toLowerCase(Locale.ROOT)) {
            case "wither":
            case "slime":
            case "ender_dragon":
            case "iron_golem":
            case "endermite":
            case "cat":
            case "player":
            case "pillager":
                return false;
        }
        return true;
    }

    public static boolean isEntityRemovable(Entity entity) {
        if(!entity.isOnGround()) {
            return false;
        }
        if(entity.getCustomName() != null && !entity.getCustomName().isEmpty()) {
            return false;
        }
        if(entity.getPassengers().size() != 0) {
            return false;
        }
        if(entity instanceof LivingEntity) {
            LivingEntity livingEntity = (LivingEntity) entity;
            if(livingEntity.isLeashed()) {
                return false;
            }
            if(entity instanceof Tameable) {
                Tameable tameable = (Tameable) entity;
                if(tameable.isTamed()) {
                    return false;
                }
            }
        }

        boolean typeSafe = isEntityTypeRemovable(entity.getType());
        if(!typeSafe) return false;

        return true;
    }
}

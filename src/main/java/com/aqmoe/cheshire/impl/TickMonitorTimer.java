package com.aqmoe.cheshire.impl;

import com.aqmoe.cheshire.Cheshire;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class TickMonitorTimer {
    public Thread mainServerThread;
    public AtomicInteger tickId;
    public AtomicLong lastTickMillisecond;
    public AtomicLong lastTickTime;

    public AtomicLong getLastTickTime() {
        return lastTickTime;
    }

    public TickMonitorTimer(Thread thread) {
        mainServerThread = thread;
        lastTickMillisecond = new AtomicLong(-1);
        tickId = new AtomicInteger(0);
        lastTickTime = new AtomicLong();
    }
    public void tick() {
        if(lastTickMillisecond.get() < 0) {
            lastTickMillisecond.set(System.currentTimeMillis());
            // Hold on. To check if this tick is lag, we should wait for next tick.
            return;
        }
        int currentTickId = tickId.addAndGet(1);
        lastTickTime.set(System.currentTimeMillis() - lastTickMillisecond.get());
        lastTickMillisecond.set(System.currentTimeMillis());
        if(currentTickId < 600) {
            // Hold on. Lag is very usual when server just start to up.
            // We should wait after 30 seconds.
            return;
        }
    }
}

package com.aqmoe.cheshire.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MemoryInformation {
    public long freeTotalMem;

    public long usedMem;

    public long totalMem;

    public long maxMem;

    public MemoryInformation() {
        int byteToMb = 1048576;
        Runtime rt = Runtime.getRuntime();
        this.totalMem = rt.totalMemory() / byteToMb;
        this.freeTotalMem = rt.freeMemory() / byteToMb;
        this.maxMem = rt.maxMemory() / byteToMb;
        this.usedMem = this.totalMem - this.freeTotalMem;
    }

    public long getFreeTotalMem() {
        return this.freeTotalMem;
    }

    public long getMaxMem() {
        return this.maxMem;
    }

    public long getTotalMem() {
        return this.totalMem;
    }

    public long getUsedMem() {
        return this.usedMem;
    }

    public double getUsedRate() {
        if (this.maxMem != 0L)
            return (double) this.totalMem / this.maxMem;
        return -1.0D;
    }

    public double getMaxUsedRate() {
        if (this.maxMem != 0L)
            return (double) this.usedMem / this.maxMem;
        return -1.0D;
    }

    public double getFreeTotalRate() {
        if (getTotalMem() != 0L)
            return (double) getUsedMem() / getTotalMem();
        return -1.0D;
    }

    public static String getPercentage(double rate) {
        BigDecimal bigDecimal = new BigDecimal(rate * 100);
        bigDecimal = bigDecimal.setScale(1, RoundingMode.HALF_UP);
        return bigDecimal.toPlainString() + "%";
    }
}
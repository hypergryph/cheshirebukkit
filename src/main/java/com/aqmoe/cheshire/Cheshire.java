package com.aqmoe.cheshire;

import com.aqmoe.aquakits.language.internal.InternalLanguagePreset;
import com.aqmoe.cheshire.managers.ConfigManager;
import com.aqmoe.cheshire.managers.DependenciesManager;
import com.aqmoe.cheshire.managers.event.BukkitEventAdopter;
import com.aqmoe.cheshire.managers.module.ModuleManager;
import com.aqmoe.cheshire.modules.OptimizedChunk;
import com.aqmoe.cheshire.modules.OptimizedEntity;
import com.aqmoe.cheshire.modules.system.ChunkStatus;
import com.aqmoe.cheshire.modules.system.ServerStatus;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public final class Cheshire extends JavaPlugin {

    static JavaPlugin instance = null;
    public static int config_version = 100;
    static DependenciesManager dependenciesManager;

    public static DependenciesManager getDependenciesManager() {
        return dependenciesManager;
    }

    @Override
    public void onEnable() {
        instance = this;
        dependenciesManager = new DependenciesManager();

        checkDependencies();

        if(getDependenciesManager().isOK()) {
            ConfigManager.load();
            Bukkit.getPluginManager().registerEvents(new BukkitEventAdopter(), this);
            loadModules();
            InternalLanguagePreset.integrate();
        }
    }

    public static Logger getBukkitLogger() {
        return instance.getLogger();
    }

    public static JavaPlugin getInstance() {
        if(instance != null) {
            return instance;
        } else {
            throw new RuntimeException("No Cheshire instance was found.");
        }
    }

    void loadModules() {
        ModuleManager.loadAllPackage("com.aqmoe.cheshire.modules");
    }

    void checkDependencies() {
        getInstance().getLogger().info("Checking dependencies...");
        getDependenciesManager().getPlugin("BKCommonLib", true);
        getDependenciesManager().getPlugin("spark", false);
    }

    @Override
    public void onDisable() {
        ModuleManager.unloadAll();
        getLogger().info("Thanks for using Cheshire.");
    }
}

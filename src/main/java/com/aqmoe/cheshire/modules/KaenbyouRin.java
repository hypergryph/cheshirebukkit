package com.aqmoe.cheshire.modules;

import com.aqmoe.aquakits.scheduler.ANormalScheduler;
import com.aqmoe.aquakits.scheduler.ScheduleType;
import com.aqmoe.cheshire.Cheshire;
import com.aqmoe.cheshire.managers.event.EventParameters;
import com.aqmoe.cheshire.managers.event.EventType;
import com.aqmoe.cheshire.managers.module.CheshireModule;
import com.aqmoe.cheshire.managers.module.ModuleRegistry;
import com.aqmoe.cheshire.modules.system.ServerStatus;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

@ModuleRegistry()
public class KaenbyouRin extends CheshireModule {

    ANormalScheduler gcScheduler;
    boolean gcLock = true;

    @Override
    public void onEnable() {
        gcScheduler = new ANormalScheduler("GCScheduler", "Request garbage collecting when server in danger every 90 seconds.", new Runnable() {
            @Override
            public void run() {
                if(ServerStatus.isInDanger()) {
                    // This timer runs every 90 seconds to check if the server is in danger and tries to trigger a GC
                    System.gc();
                } else if (Bukkit.getOnlinePlayers().size() == 0 && gcLock) {
                    // We will also try to trigger GC automatically when there is no player
                    // The frequency is controlled, i.e., automatic GC trigger will run at most once every 30 minutes.
                    System.gc();
                    gcLock = false;
                    Bukkit.getScheduler().runTaskLater(Cheshire.getInstance(), () -> { gcLock = true; }, (20*60*30));
                }
            }
        }, ScheduleType.TIMER_ASYNC, 20, (20*90));

        gcScheduler.start(Cheshire.getInstance());
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    @Override
    public void onListening(EventType type, Event bukkit_event, EventParameters parameters) {
        switch (type) {
            case ServerInDanger:
                // Triggers GC immediately after the server is in danger
                System.gc();
        }
    }
}

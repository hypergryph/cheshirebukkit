package com.aqmoe.cheshire.modules;

import com.aqmoe.aquakits.java.SafeNumberMapModel;
import com.aqmoe.aquakits.minecraft.world.WrappedChunk;
import com.aqmoe.aquakits.scheduler.ANormalScheduler;
import com.aqmoe.aquakits.scheduler.ScheduleType;
import com.aqmoe.cheshire.Cheshire;
import com.aqmoe.cheshire.managers.ConfigManager;
import com.aqmoe.cheshire.managers.event.EventParameters;
import com.aqmoe.cheshire.managers.event.EventType;
import com.aqmoe.cheshire.managers.event.EventsManager;
import com.aqmoe.cheshire.managers.module.CheshireModule;
import com.aqmoe.cheshire.managers.module.ModuleRegistry;
import com.aqmoe.cheshire.modules.system.ChunkStatus;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.Event;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.world.WorldLoadEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@ModuleRegistry()
public class OptimizedChunk extends CheshireModule {

    public static void unloadChunks(List<Chunk> chunks, boolean gracefully) {
        for(Chunk chunk : chunks) {
            if(gracefully) {
                chunk.getWorld().unloadChunkRequest(chunk.getX(), chunk.getZ());
            } else {
                chunk.getWorld().unloadChunk(chunk);
            }
        }
    }

    @Override
    public void onEnable() {
        EventsManager.register(EventType.WorldLoad, this);
    }

    @Override
    public void onDisable() {
    }

    @Override
    public void onListening(EventType type, Event bukkit_event, EventParameters parameters) {
        ConfigurationSection conf = ConfigManager.getModuleConfig("OptimizedChunk");
        switch (type) {
            case WorldLoad:
                Bukkit.getScheduler().runTask(Cheshire.getInstance(), () -> {
                    WorldLoadEvent event = (WorldLoadEvent) bukkit_event;
                    World world = event.getWorld();
                    if(!conf.getStringList("disable-spawn-chunk.exclude").contains(world.getName())) {
                        world.setKeepSpawnInMemory(false);
                        getLogger().debug(world.getName() + "'s setKeepSpawnInMemory has been set to false");
                    }
                });
                break;
        }
    }

    @Deprecated
    void autoUnloadChunks() {
        ArrayList<Chunk> requestedChunks = new ArrayList<>();
        for(World world : Bukkit.getWorlds()) {
            for(Chunk chunk : world.getLoadedChunks()) {
                WrappedChunk wrappedChunk = new WrappedChunk(chunk);
                if(!ChunkStatus.isChunkActive(chunk) && !wrappedChunk.isForceLoaded() && !chunk.isSlimeChunk()) {
                    requestedChunks.add(chunk);
                }
            }
        }
        unloadChunks(requestedChunks, true);
        getLogger().debug("Attempted to auto-unload " + requestedChunks.size() + " chunks.");
    }
}

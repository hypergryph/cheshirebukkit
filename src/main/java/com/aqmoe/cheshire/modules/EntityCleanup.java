package com.aqmoe.cheshire.modules;

import com.aqmoe.aquakits.exceptions.HonMeirin;
import com.aqmoe.aquakits.scheduler.ANormalScheduler;
import com.aqmoe.aquakits.scheduler.ScheduleType;
import com.aqmoe.cheshire.Cheshire;
import com.aqmoe.cheshire.impl.category.CreatureCategories;
import com.aqmoe.cheshire.impl.category.CreatureCategory;
import com.aqmoe.cheshire.impl.category.SafetyCheck;
import com.aqmoe.cheshire.managers.ConfigManager;
import com.aqmoe.cheshire.managers.event.EventParameters;
import com.aqmoe.cheshire.managers.event.EventType;
import com.aqmoe.cheshire.managers.event.EventsManager;
import com.aqmoe.cheshire.managers.module.CheshireModule;
import com.aqmoe.cheshire.managers.module.ModuleRegistry;
import com.aqmoe.cheshire.modules.system.ChunkStatus;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.Event;

import java.util.*;

@ModuleRegistry()
public class EntityCleanup extends CheshireModule {

    ANormalScheduler cleanupScheduler;
    ANormalScheduler advancedCleanupScheduler;

    public static void cleanUpEntities(Collection<Entity> entities) {
        cleanUpEntities(entities, false, false, null);
    }

    public static void cleanUpEntities(Collection<Entity> entities, boolean ignoreTypeCheck, boolean ignoreActiveCheck, CreatureCategory specificCategory) {
        ConfigurationSection c = ConfigManager.getModuleConfig("EntityCleanup");
        ArrayList<Entity> pending = new ArrayList<>();
        for(Entity entity : entities) {
            if(ChunkStatus.isChunkActive(entity.getLocation().getChunk())) {
                if(!ignoreActiveCheck) {
                    continue;
                }
            }
            if(!SafetyCheck.isEntityRemovable(entity)) {
                continue;
            }
            CreatureCategory category = CreatureCategories.recognize(entity);
            if(category != CreatureCategory.NotRecognized) {
                if(specificCategory != null) {
                    if(specificCategory == category) {
                        pending.add(entity);
                    }
                    continue;
                } else {
                    String confPath = "clean-up-types." + category.name().toLowerCase(Locale.ROOT);
                    if (ignoreTypeCheck || !c.contains(confPath) /* 如果该配置不存在则默认清理, 比如 monster */ || c.getBoolean(confPath)) {
                        pending.add(entity);
                    }
                }

            }
        }

        OptimizedEntity.removeEntities(pending);
    }

    @Override
    public void onEnable() {

        EventsManager.register(EventType.ServerInDanger, this);

        if(getConfig().getBoolean("auto-clean-up.enabled")) {
            int ticks = getConfig().getInt("auto-clean-up.interval") * 20;
            cleanupScheduler = new ANormalScheduler("AutoCleanUp",
                    "Attempt to automatically clean up unwanted entities for a specified time interval",
                    new Runnable() {
                        @Override
                        public void run() {
                            autoCleanup();
                        }
                    },
                    ScheduleType.TIMER_ASYNC, 20, ticks);
            cleanupScheduler.start(Cheshire.getInstance());
        }

        ConfigurationSection c = getConfig();
        int advanced_interval = c.getInt("advanced-clean-up.interval");
        boolean advanced_enabled = c.getBoolean("advanced-clean-up.enabled");

        if(advanced_enabled) {
            advancedCleanupScheduler = new ANormalScheduler("AdvancedCleanup",
                    "Attempt to automatically clean up entities every " + advanced_interval + " seconds", new Runnable() {
                @Override
                public void run() {
                    cleanUpAllEntities();
                }
            }, ScheduleType.TIMER_ASYNC, (20*advanced_interval), (20*advanced_interval));
            advancedCleanupScheduler.start(Cheshire.getInstance());
        }


    }

    public static void cleanUpAllEntities() {
        for(World world : Bukkit.getWorlds()) {
            List<Entity> entities;
            try {
                entities = Bukkit.getScheduler().callSyncMethod(Cheshire.getInstance(), world::getEntities).get();
            } catch (Exception e) {
                return;
            }
            cleanUpEntities(entities);
        }
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    @Override
    public void onListening(EventType type, Event bukkit_event, EventParameters parameters) {
        switch (type) {
            case ServerInDanger:
                cleanUpAllEntities();
        }
    }

    public void autoCleanup() {
        ConfigurationSection cleanTypes = getConfig().getConfigurationSection("auto-clean-up.target");
        ArrayList<Entity> pendingRemoveEntities = new ArrayList<>();
        for(World world : Bukkit.getWorlds()) {
            for(Entity entity : world.getEntities()) {
                if(ChunkStatus.isChunkActive(entity.getLocation().getChunk())) {
                    continue;
                }

                if(!SafetyCheck.isEntityRemovable(entity)) {
                    continue;
                }

                if(cleanTypes.getBoolean("boat")) {
                    if(entity instanceof Boat) {
                        Boat boat = (Boat) entity;
                        if(boat.getPassengers().size() == 0) {
                            pendingRemoveEntities.add(entity);
                            continue;
                        }
                    }
                }
                if(cleanTypes.getBoolean("arrow_in_ground")) {
                    if(entity instanceof Arrow) {
                        Arrow arrow = (Arrow) entity;
                        if(arrow.isInBlock() || arrow.isOnGround()) {
                            pendingRemoveEntities.add(entity);
                            continue;
                        }
                    }
                }
                if(cleanTypes.getBoolean("experience_orb")) {
                    if(entity instanceof ExperienceOrb) {
                        pendingRemoveEntities.add(entity);
                        continue;
                    }
                }
                if(cleanTypes.getBoolean("falling_block")) {
                    if(entity instanceof FallingBlock) {
                        pendingRemoveEntities.add(entity);
                        continue;
                    }
                }
            }
        }
        OptimizedEntity.removeEntities(pendingRemoveEntities);
        if(getConfig().getBoolean("auto-clean-up.log")) {
            getLogger().info("静默清理了" + pendingRemoveEntities.size() + "个实体，感觉自己棒棒哒~");
        }
    }

    @Deprecated
    void autoCleanupChunks() {
        int cleanTypes = getConfig().getInt("busy-point-threshold");
        for(Chunk chunk : ChunkStatus.busyPointChunks.keySet()) {
            if(ChunkStatus.getChunkBusyPoints(chunk) >= cleanTypes) {
                Entity[] entities;
                try {
                    entities = Bukkit.getScheduler().callSyncMethod(Cheshire.getInstance(), chunk::getEntities).get();
                } catch (Exception e) {
                    return;
                }
                cleanUpEntities(Arrays.asList(entities), false, false, null);
            }
        }
    }
}

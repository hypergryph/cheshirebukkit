package com.aqmoe.cheshire.modules;

import com.aqmoe.aquakits.java.SafeNumberMapModel;
import com.aqmoe.aquakits.minecraft.world.WrappedChunk;
import com.aqmoe.aquakits.scheduler.ANormalScheduler;
import com.aqmoe.aquakits.scheduler.ScheduleType;
import com.aqmoe.cheshire.Cheshire;
import com.aqmoe.cheshire.impl.category.CreatureCategories;
import com.aqmoe.cheshire.impl.category.CreatureCategory;
import com.aqmoe.cheshire.impl.category.SafetyCheck;
import com.aqmoe.cheshire.managers.ConfigManager;
import com.aqmoe.cheshire.managers.event.EventParameters;
import com.aqmoe.cheshire.managers.event.EventType;
import com.aqmoe.cheshire.managers.event.EventsManager;
import com.aqmoe.cheshire.managers.module.CheshireModule;
import com.aqmoe.cheshire.managers.module.ModuleManager;
import com.aqmoe.cheshire.managers.module.ModuleRegistry;
import com.aqmoe.cheshire.modules.system.ChunkStatus;
import com.aqmoe.cheshire.modules.system.ServerStatus;
import com.bergerkiller.bukkit.common.events.CreaturePreSpawnEvent;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.Event;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDeathEvent;

import java.util.*;

@ModuleRegistry()
public class OptimizedEntity extends CheshireModule {

    ArrayList<Chunk> activeSpawnChunks = new ArrayList<>();
    SafeNumberMapModel liveTime = new SafeNumberMapModel();
    ANormalScheduler liveTimeChecker;
    ANormalScheduler activeScheduler;

    boolean isChunkActive2Spawn(Chunk chunk) {
        return activeSpawnChunks.contains(chunk);
    }

    public static void removeEntities(List<Entity> entities) {
        Bukkit.getScheduler().runTask(Cheshire.getInstance(), () -> {
            for(Entity entity : entities) {
                entity.remove();
            }
            if(entities.size() > 0) ModuleManager.getModule("OptimizedEntity").getLogger().debug("Removed " + entities.size() + " entities");
        });
    }

    @Override
    public void onEnable() {
        EventsManager.register(EventType.CreaturePreSpawn, this);
        EventsManager.register(EventType.CreatureSpawn, this);
        EventsManager.register(EventType.EntityDeath, this);
        EventsManager.register(EventType.CreatureCategoryReachedLimit, this);

        liveTimeChecker = new ANormalScheduler("LiveTimeChecker",
                "Check monster's live time and remove the exceeded ones", () -> {
                    ArrayList<Entity> pendingRemove = new ArrayList<>();
                    for(String uuid : liveTime.map.keySet()) {
                        Entity entity = Bukkit.getEntity(UUID.fromString(uuid));
                        if(entity == null || !entity.isValid() || entity.isDead()) {
                            liveTime.map.remove(uuid);
                        } else {
                            liveTime.add(uuid, -1);
                            getLogger().debug("Monster " + entity.getType() + " currently has a survival time of " + liveTime.get(uuid));
                            if(liveTime.get(uuid) <= 0) {
                                if(ServerStatus.isInDanger() || !isChunkActive2Spawn(entity.getLocation().getChunk())) {
                                    pendingRemove.add(entity);
                                    liveTime.map.remove(uuid);
                                }

                            }
                        }
                    }
                    removeEntities(pendingRemove);
                }, ScheduleType.TIMER_ASYNC, 200, 20);

        liveTimeChecker.start(Cheshire.getInstance());

        activeScheduler = new ANormalScheduler("EntitySpawnActiveChunkStatus",
                "Chunks are analyzed every 4 seconds to find active ones" +
                        "that can spawn living entities.",
                () -> {
                    ArrayList<Chunk> newActiveChunks = new ArrayList<>();
                    int radius = ConfigManager.getModuleConfig(getName()).getInt("active-radius");
                    for(Player player : Bukkit.getOnlinePlayers()) {
                        newActiveChunks.addAll(WrappedChunk.around(player.getLocation().getChunk(), radius, radius));
                    }
                    activeSpawnChunks = newActiveChunks;
                },
                ScheduleType.TIMER_ASYNC, 20, (20*4)
        );

        activeScheduler.start(Cheshire.getInstance());
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    @Override
    public void onListening(EventType type, Event bukkit_event, EventParameters parameters) {
        switch (type) {
            case CreaturePreSpawn:
                onCreaturePreSpawnCheck((CreaturePreSpawnEvent) bukkit_event);
                break;
            case CreatureSpawn:
                onCreatureSpawn((CreatureSpawnEvent) bukkit_event);
                break;
            case EntityDeath:
                onCreatureDeath((EntityDeathEvent) bukkit_event);
                break;
            case CreatureCategoryReachedLimit:
                CreatureCategory category = (CreatureCategory) parameters.get("category");
                Chunk attemptChunk = (Chunk) parameters.get("chunk");
                onReachedLimitAutoClean(category, attemptChunk);
                break;
        }
    }

    void onReachedLimitAutoClean(CreatureCategory category, Chunk attemptChunk) {
        if(getConfig().getBoolean("auto-clean")) {
            getLogger().debug("Trying to auto-clean...");
            EntityCleanup.cleanUpEntities(Arrays.asList(attemptChunk.getEntities()), true, true, category);
        }
    }

    /**
     * 当 bukkit 尝试生成新实体时，做两项检查：
     * 1) 是否是在“生成活跃”的区块内（这一点是OptimizedEntity中特别定义的，和默认的不同）
     * 2) 是否超过了单个区块内的分类实体数量限制
     */
    void onCreaturePreSpawnCheck(CreaturePreSpawnEvent event) {
        Chunk attemptChunk = event.getSpawnLocation().getChunk();
        if(!SafetyCheck.isEntityTypeRemovable(event.getEntityType())) {
            getLogger().debug("Bypassed the pre-spawn check due to security reason for "
                    + event.getEntityType().name());
            return;
        }

        if(
                ConfigManager.getModuleConfig("OptimizedEntity").getBoolean("limit-spawn-attempt")
        ) {
            if(!isChunkActive2Spawn(attemptChunk)) {
                event.setCancelled(true);
                getLogger().debug("Cancelled a creature to be spawned due to non-active chunk limit: "
                        + event.getEntityType().name() + " (reason: " + event.getSpawnReason().name() + ")");
            }
        }

        boolean allowOrNot = checkCategoryLimitation(event.getEntityType(), attemptChunk);
        if(!allowOrNot){
            event.setCancelled(true);
            getLogger().debug("Cancelled a creature to be spawned due to overuse: "
                    + event.getEntityType().name() + " (reason: " + event.getSpawnReason() + ")");
        }
    }

    /**
     * 检查分类实体数量限制
     * @param entityType bukkit entity type
     * @param attemptChunk the chunk requested to check the limitation for
     * @return boolean 是否允许生成
     */
    private boolean checkCategoryLimitation(EntityType entityType, Chunk attemptChunk) {
        CreatureCategory category = ChunkStatus.entityTypeToCategory.get(entityType.name());
        if(category != null) {
            // 现在我们成功通过 EntityType 获取到了对应的 Category
            return !ChunkStatus.isChunkReachedCategoryLimitation(category, attemptChunk);
        }
        return true;
    }

    /**
     * 在 bukkit 提供的 CreatureSpawn 中，我们要做一项检查：
     *  - 重复检查是否越过实体数量限制，因为 CreaturePreSpawn 不能覆盖全部的生物生成事件
     *  我们另外要生成一项数据：
     *   - 如果是怪物，那么给这个实体生成生存时间
     *   - 是否禁用其 AI
     *
     */
    void onCreatureSpawn(CreatureSpawnEvent event) {
        ConfigurationSection c = ConfigManager.getModuleConfig(this);
        // 我们在这里进行一个对实体数量限制的二次检查
        // 因为 PreSpawn 只有 bukkit 自动生成时有效
        boolean result = checkCategoryLimitation(event.getEntityType(), event.getEntity().getLocation().getChunk());
        if(!result) {
            event.getEntity().remove();
            return;
        }
        CreatureCategory category = CreatureCategories.recognize(event.getEntity());
        // 在这里，我们对任何刚刚生成的怪物初始化生存时间
        if(category == CreatureCategory.Monster) {
            if(!SafetyCheck.isEntityRemovable(event.getEntity())) {
                // 如果实体不可以被移除，不给他设置生存时间就 OK
                return;
            }

            liveTime.set(event.getEntity().getUniqueId().toString(), c.getInt("monster-max-survival-time"));
        }

        // AI 设定
        if(c.contains("ai." + category.name()) && !c.getBoolean("ai." + category.name())) {
            event.getEntity().setAI(false);
        }

    }


    void onCreatureDeath(EntityDeathEvent event) {
        // 在这里，我们对这个死亡了的怪物删除生存时间
        if(CreatureCategories.recognize(event.getEntity()) == CreatureCategory.Monster) {
            liveTime.map.remove(event.getEntity().getUniqueId().toString());
        }
    }
}

package com.aqmoe.cheshire.modules.system;

import com.aqmoe.aquakits.scheduler.ANormalScheduler;
import com.aqmoe.aquakits.scheduler.ScheduleType;
import com.aqmoe.aquakits.scheduler.SchedulerRegistry;
import com.aqmoe.cheshire.Cheshire;
import com.aqmoe.cheshire.impl.TickMonitorTimer;
import com.aqmoe.cheshire.managers.event.EventParameters;
import com.aqmoe.cheshire.managers.event.EventType;
import com.aqmoe.cheshire.managers.event.EventsManager;
import com.aqmoe.cheshire.managers.module.CheshireModule;
import com.aqmoe.cheshire.managers.module.ModuleRegistry;
import com.aqmoe.cheshire.utils.MemoryInformation;
import com.aqmoe.cheshire.utils.TPSRunnable;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import java.io.File;

import static com.aqmoe.cheshire.utils.DirectorySize.size;

@ModuleRegistry(system = true)
public class ServerStatus extends CheshireModule {

    public ANormalScheduler statusScheduler;

    public ANormalScheduler tickScheduler;

    TickMonitorTimer tickMonitorTimer;

    public static boolean lagging = false;

    public static boolean lastStatus = false;

    public static int dangerCount = 0;

    public static boolean lackOfMemory = false;

    public static long mbDiskUsed = 0;

    public static boolean isLagging() {
        return lagging;
    }

    public static long getMbDiskUsed() {
        return mbDiskUsed;
    }

    public static boolean isLackOfMemory() {
        return lackOfMemory;
    }

    public static int getDangerCount() {
        return dangerCount;
    }

    public static boolean isInDanger() {
        // 当内存不足和TPS不足同时出现超过15s时，标记服务器为危险（虽然很傻，但是暂时只能写到这）
        return dangerCount > 6;
    }

    public static long fetchMbStorageUsed() {
        File f = Bukkit.getServer().getWorldContainer();
        long size = size(f.toPath());
        size = size / (1024 * 1024);
        return size;
    }

    @Override
    public void onEnable() {
        tickMonitorTimer = new TickMonitorTimer(Thread.currentThread());
        statusScheduler = new ANormalScheduler("ServerStatus",
                "Checking the fluency of the server in order to " +
                        "provide a unique indicator for other modules to use for checking every 5 seconds",
                new Runnable() {
                    @Override
                    public void run() {
                        if (TPSRunnable.getTPS() < 18) {
                            if(lagging) {
                                dangerCount ++;
                            }
                            lagging = true;
                        } else {
                            if(dangerCount >= 1) dangerCount -= 1;
                        }
                        MemoryInformation mi = new MemoryInformation();
                        if (mi.getMaxUsedRate() > 0.8) {
                            if(lackOfMemory) {
                                dangerCount ++;
                            }
                            lackOfMemory = true;
                        } else {
                            if(dangerCount >= 1) dangerCount -= 1;
                        }
                        mbDiskUsed = fetchMbStorageUsed();

                        if(isInDanger()) {
                            if(!lastStatus) {
                                // Just start lagging. Fire the event.
                                EventsManager.fire(EventType.ServerInDanger);
                                getLogger().debug("Server in danger!");
                                lastStatus = true;
                            }
                        } else {
                            lastStatus = false;
                        }

                    }
                },
                ScheduleType.TIMER_ASYNC, 20, 100
        );

        statusScheduler.start(Cheshire.getInstance());

        tickScheduler = new ANormalScheduler("TickStatus",
                "Checking the fluency of the server in order to " +
                        "provide a unique indicator for other modules to use for checking every 5 seconds",
                new Runnable() {
                    @Override
                    public void run() {
                        tickMonitorTimer.tick();
                    }
                },
                ScheduleType.TIMER_ASYNC, 20, 1
        );

        tickScheduler.start(Cheshire.getInstance());
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    @Override
    public void onListening(EventType type, Event bukkit_event, EventParameters parameters) {
        super.onListening(type, bukkit_event, parameters);
    }
}

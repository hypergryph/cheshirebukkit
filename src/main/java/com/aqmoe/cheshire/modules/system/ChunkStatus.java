package com.aqmoe.cheshire.modules.system;

import com.aqmoe.aquakits.exceptions.HonMeirin;
import com.aqmoe.aquakits.java.SafeNumberMapModel;
import com.aqmoe.aquakits.minecraft.world.WrappedChunk;
import com.aqmoe.aquakits.scheduler.ANormalScheduler;
import com.aqmoe.aquakits.scheduler.ScheduleType;
import com.aqmoe.cheshire.Cheshire;
import com.aqmoe.cheshire.impl.category.CreatureCategories;
import com.aqmoe.cheshire.impl.category.CreatureCategory;
import com.aqmoe.cheshire.managers.ConfigManager;
import com.aqmoe.cheshire.managers.event.EventParameters;
import com.aqmoe.cheshire.managers.event.EventType;
import com.aqmoe.cheshire.managers.event.EventsManager;
import com.aqmoe.cheshire.managers.module.CheshireModule;
import com.aqmoe.cheshire.managers.module.ModuleRegistry;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@ModuleRegistry(system = true)
public class ChunkStatus extends CheshireModule {

    public ANormalScheduler statusScheduler;
    public ANormalScheduler activeScheduler;

    public int currentChunks;

    public static HashMap<Chunk, Integer> busyPointChunks = new HashMap<>();
    public static ConcurrentHashMap<CreatureCategory, ArrayList<Chunk>> overrunChunks = new ConcurrentHashMap<>();
    public static ArrayList<Chunk> activeChunks = new ArrayList<>();
    public static HashMap<String, CreatureCategory> entityTypeToCategory = new HashMap<>();

    public int getCurrentChunks() {
        return currentChunks;
    }

    void analyzeChunk(Chunk chunk) {
        // Analysis of chunks.
        // Generate a "Busy Point", which is used to measure the resources occupied by the chunks.
        // Also check if the chunk has reached its spawn limit

        if(!Cheshire.getInstance().isEnabled()) return;

        Entity[] entities;
        BlockState[] tilesEntities;

        try {
            entities =
                    Bukkit.getScheduler().callSyncMethod(Cheshire.getInstance(), chunk::getEntities).get();
            tilesEntities =
                    Bukkit.getScheduler().callSyncMethod(Cheshire.getInstance(), chunk::getTileEntities).get();
        } catch (Exception e) {
            return;
        }

        int entityCount = entities.length;
        int tileEntityCount = tilesEntities.length;

        int busyPoint = (entityCount) + (tileEntityCount * 2);
        busyPointChunks.put(chunk, busyPoint);

        SafeNumberMapModel statistics = new SafeNumberMapModel();
        for(Entity entity : entities) {
            CreatureCategory creatureCategory = CreatureCategories.recognize(entity);
            if(creatureCategory != CreatureCategory.NotRecognized) {
                statistics.add(creatureCategory.name(), 1);

                // 在这里我们生成一个从 EntityType 到 Category 的映射
                // 方便我们在预生成事件时使用，那时不能获取到 Entity
                if(!entityTypeToCategory.containsKey(entity.getType().name())) {
                    entityTypeToCategory.put(entity.getType().name(), creatureCategory);
                    getLogger().debug("Recognized " + entity.getType() + " to category " + creatureCategory);
                }
            }
        }

        // Attention! 这里我们需要访问 OptimizedEntity 的配置

        ConfigurationSection c = ConfigManager.getModuleConfig("OptimizedEntity");

        for(String category : statistics.map.keySet()) {
            CreatureCategory thatCategory = CreatureCategory.valueOf(category);
            int limit = c.getInt("spawn-limit." + category.toLowerCase(Locale.ROOT), 0);
            if(limit >= 0) {
                if(limit <= statistics.get(category)) {
                    if (!overrunChunks.containsKey(thatCategory)) {
                        overrunChunks.put(thatCategory, new ArrayList<>());
                    }

                    if(!overrunChunks.get(thatCategory).contains(chunk)) {
                        // 不要重复触发这个事件
                        EventsManager.fire(EventType.CreatureCategoryReachedLimit, null,
                                new EventParameters().add("category", thatCategory)
                                        .add("chunk", chunk));
                        overrunChunks.get(thatCategory).add(chunk);
                    }

                } else {
                    if (!overrunChunks.containsKey(thatCategory)) {
                        overrunChunks.put(thatCategory, new ArrayList<>());
                    } else {
                        overrunChunks.get(thatCategory).remove(chunk);
                    }
                }
            }
        }
    }

    public static boolean isChunkReachedCategoryLimitation(CreatureCategory category, Chunk chunk) {
        if(overrunChunks.containsKey(category)) {
            return overrunChunks.get(category).contains(chunk);
        } else {
            return false;
        }
    }

    public static Integer getChunkBusyPoints(Chunk chunk) {
        return busyPointChunks.get(chunk);
    }

    public static boolean isChunkActive(Chunk chunk) {
        return activeChunks.contains(chunk);
    }

    @Override
    public void onEnable() {
        busyPointChunks = new HashMap<>();
        statusScheduler = new ANormalScheduler("ChunkStatus",
                "Check the chunk status in real time, " +
                        "and try to clean up when some chunks are relatively overloaded",
                new Runnable() {
                    @Override
                    public void run() {
                        int count = 0;
                        for(World world : Bukkit.getWorlds()) {
                            for(Chunk chunk : world.getLoadedChunks()) {
                                count ++;
                                analyzeChunk(chunk);
                            }
                        }
                        currentChunks = count;
                    }
                },
                ScheduleType.TIMER_ASYNC, 20, 200
        );

        statusScheduler.start(Cheshire.getInstance());

        activeScheduler = new ANormalScheduler("ChunkActiveStatus",
                "Check if chunk is in active (players nearby 3x3) every 4 seconds",
                new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<Chunk> newActiveChunks = new ArrayList<>();
                        for(Player player : Bukkit.getOnlinePlayers()) {
                            for(Chunk chunk : WrappedChunk.around(player.getLocation().getChunk(), 3, 3)) {
                                newActiveChunks.add(chunk);
                            }
                        }
                        activeChunks = newActiveChunks;
                    }
                },
                ScheduleType.TIMER_ASYNC, 20, (20*4)
        );

        activeScheduler.start(Cheshire.getInstance());

    }

    @Override
    public void onDisable() {
        activeScheduler.cancel();
        statusScheduler.cancel();
    }

    @Override
    public void onListening(EventType type, Event bukkit_event, EventParameters parameters) {

    }
}
